

// commenting style in JS
// single line comment with double forward slashes
/*
    multi
    line
    comment
*/

// general programming concept
// Operators
// arthemetic operators,(addition,substraction) +
// relational operators > <
// assginmenet  =
// comparisiion operator ==, ===
// conditional condition ? <truth_statement> : <false_statement> (ternary operator)
// concatinate operator , + comma
// logical operator   
// && and (if X and Y given both X and Y must be true)
// || or (if X and Y given) either X can be true or Y can true
// ! if true evaluates to false and viceversa
// bitwise operator  & , |, ~


// Conditionals 
// if 
// if -else
// if else if else
// switch

// loops (iterations)
// repetation of code till certain condition is fullfilled
// for  ==> count specific 

// while, do-while  ==> condition specific
// JS specific loop
// for in, for of,==>
// forEach map,filter, reduce,some ,every,find ==> 

// variable and constants
// memory allocation to hold values

// js way of defining variables
// let or var keyword for variables

// const emai;
// const phone;

// var name;
// var address

// let roll_no;
// let id;

// const keyword for constant
// functions==> resusable block of code

// if(condition){

// }else{

// }
// switch(){

// }




// datatype  datatype is classificaiton of data
// programming language wise classificiton will be different

// JS way of classfication

// 2 group of data type
// 1. Primitive Datatype
// 2. non primitive datatype


// primitive datatype
// String ==> single quote or double quote enclosed value are string
// eg 'kishor',"sagarmatha" '222' "true "
// Number==>33.2223, 22, 
// Boolean ==> true or false
// Undefined,==> memory allocated value not definedn
// Null  ==> non allocated memory are referred as null
// ----------------------------------------------------------
// symbol  ==> unique value
// biginit ===>

// eg
// var address = 'sanepa';
// let phone = 3333;
// const email = "sagarmatha@gmail.com";
// const isOpen = true;

var xyz;
// xyz = 'sagarmatha';
// xyz = 3333;
// xyz = true

// in JS we dont have to define datatype when allocating memory
// IS is weakly typed programming language
// eg in other PL int a, float b, char c;


// print 
// console.log('here test data type >>>>',xyz)


// JS version

// Es5,Es6

// stable JS , modern JS
// es5   || es7 and beyond


// primtiive data type are generally used to hold single value
var addr = 'sanepa';
var full_name = 'kishor giri';
var phonenumbers='333,555,666'



// var laptop = {
//         name:'xps',
//         brand:'dell',
//         ram:6,
//         color:'black'
// }

